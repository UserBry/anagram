const button = document.getElementById("findButton");

button.onclick = function ()
{
    let typedText = document.getElementById("input").value;

    function alphabetize(a)
    {
        return a.toLowerCase().split("").sort().join("").trim();
    }

    let app = alphabetize(typedText);


    let elppa = [];
    let bee = [];
    let flag = 0;
    for(i = 0; i < words.length; i++)
    {
        elppa.push(alphabetize(words[i]));  //alphaArray
        if( app == elppa[i])        //if we match anagram
        {
            if(typedText != words[i])   //if typedText isn't the same word
            {
                bee.push(words[i]);     //push it onto bee array
                flag = 1;
            }
        }
    }        

    if(flag == 1)
    {
        const newElement = document.createElement("p");
        newElement.style.color = "green";
        const newText = document.createTextNode("These are anagrams for (" + typedText + "): " + bee);
        newElement.appendChild(newText);
        document.getElementById("this").appendChild(newElement);
    }
    else
    {
        const newElement = document.createElement("p");
        newElement.style.color = "brown";
        const newText = document.createTextNode("There are no anagrams for: " + typedText);
        newElement.appendChild(newText);
        document.getElementById("this").appendChild(newElement);
    }
    
}
